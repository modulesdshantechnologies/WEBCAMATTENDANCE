var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'),
    UserModel = mongoose.model('user');

module.exports = function (app){
    app.use('/', router);
};

router.post('/userRegistration', function(req, res, next) {
    var userModel = new UserModel(req.body);
    userModel.save(function(err, result) {
        if (err){
            console.log('Registration failed: ' + err);
        }
        res.send(result);
    });
});

router.post('/login', function(req, res, next) {
   console.log(">>>>", req.body);
    var email = req.body.email;
    var password =  req.body.password;
    UserModel.findOne({email:email}, {password:1},  function(err, result) {
        if (err){
            res.send({loginStatus: false});
        }
        if(password === result.password ){
            res.send({loginStatus: true});
        }
        else{
            res.send({loginStatus: false});
        }
    });
});

