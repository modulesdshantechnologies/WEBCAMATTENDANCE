var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'),
    UserModel = mongoose.model('register');

module.exports = function (app){
    app.use('/', router);
};

var saveImageAsFile = function(data, fileName){
    var base64Data = data.replace(/^data:image\/jpeg;base64,/, "");
    require("fs").writeFile("public/photos/" + fileName + ".jpg", base64Data, {encoding: 'base64'}, function(err) {
        if(err){
            console.log("photo save failed: ", err);
            return;
        }
        console.log('File created');
        return;
    });
};
router.post('/Registration', function(req, res, next) {
    var photos = req.body.photo;
    photos.forEach(function(photo, index){
        saveImageAsFile(photo, req.body.name + '_' + index+1);
    });

    var user = req.body;
    user.photo = [];
    photos.forEach(function(photo, index){
        user.photo.push(req.body.name + '_' + index+1 + ".jpg");
    });
    var userModel = new UserModel(user);
    userModel.save(function(err, result) {
        if (err){
            console.log('Registration failed: ' + err);
        }
        res.send(result);
    });
});


router.get('/registerBymongoId/:registerMongoId',function(req,res,next){
console.log('registerMongoId', req.params.registerMongoId);
UserModel.findOne({"_id":req.params.registerMongoId},function(err,result){
                    if(err)
                        {
                         console.log(err);
                        }
                     else
                      {

                         console.log("Result:", result);
                         res.send(result);

                        }


                       })

})

router.post('/editRegisterBymongoId', function(req, res, next) {
console.log("******", req.body)
console.log(req.body._id);
saveImageAsFile(req.body.photo, req.body.name);
var user = req.body;
user.photo = req.body.name + ".jpg";
UserModel.findOneAndUpdate({"_id":req.body._id},req.body,{upsert: true, new: true},
function(err,result)
    {
        if(err){
            console.log(err.stack)
        }else{
            res.send(result)
        }

    });

})


router.delete('/registerBymongoId/:registerMongoId',function(req, res, next){
console.log('registerMongoId', req.params.registerMongoId);
UserModel.remove({"_id":req.params.registerMongoId},function(err,result)
{
if(err)
{
 console.log(err);
}
else
{
 res.send(result)
}

});
});


router.get('/AllRegisterDetails', function(req, res, next) {
UserModel.find({},function(err,result){
        if(err){
            res.send(err)
            console.log(err.stack)
        }else{
            res.send(result)
        }

    }).skip(parseInt(req.params.start)).limit(parseInt(req.params.range))

})

