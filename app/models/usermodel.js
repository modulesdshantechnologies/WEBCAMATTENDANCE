var mongoose = require('mongoose'),
  Schema = mongoose.Schema;
var EventResultSchema = new mongoose.Schema(
  {
    userName:String,
    firstName:String,
    lastName:String,
    email:String,
    password:String,
    role:String
},
{collection:"user"});
mongoose.model('user',EventResultSchema);