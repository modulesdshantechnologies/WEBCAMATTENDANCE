webapp.factory('userServices',function($http){

    var postAllRegisterDetails = function(data)
    {
       return $http.post('/userRegistration', data);
    };

    var postAllLoginDetails = function(data)
    {
        console.log("testing:::::", data)
       return $http.post('/login', data);
    };

    return {
      postAllRegisterDetails:postAllRegisterDetails,
      postAllLoginDetails: postAllLoginDetails
    }

});
