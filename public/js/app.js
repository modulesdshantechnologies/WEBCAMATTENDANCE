var webapp = angular.module('webapp', ['ui.router','ng-webcam','angularUtils.directives.dirPagination']);

   webapp.config(function($stateProvider, $urlRouterProvider) {

       $urlRouterProvider.otherwise('/register');

       $stateProvider

       // HOME STATES AND NESTED VIEWS ========================================
           .state('register', {
               url: '/register',
               templateUrl: 'templates/register.html',
               controller:'userController'

           })

           .state('webcam', {
              url: '/webcam',
              templateUrl: 'templates/webcam.html',
              controller:'webcamController'
           })

           .state('table', {
             url: '/table/:registerId',
             templateUrl: 'templates/table.html',
             controller:'tableController'
           })

           .state('updatetable', {
                url: '/updatetable/:registerId',
                templateUrl: 'templates/updatetable.html',
                controller:'updateTableController'
            })

           // ABOUT PAGE AND MULTIPLE NAMED VIEWS =================================
           .state('login', {
               url: '/login',
               templateUrl: 'templates/login.html',
               controller:'userController'

               // we'll get to this in a bit
           });

   });